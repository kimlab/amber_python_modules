#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 14:58:37 2017

@author: owenwhitley
"""


import os
import argparse
import subprocess

parser = argparse.ArgumentParser(prog = "Rosetta -> Amber pdb conversion",
                        description = '''
Generate properly formatted pdb for amber from rosetta generated pdb                  
                  ''')
parser.add_argument('-pdb','--pdb_folder', required = True, help = 'folder containing all input files')
parser.add_argument('-out','--output_folder', required = True, help = 'folder containing all output subdirectories')
parser.add_argument('-ambdir','--amber_projects_dir', required = True, help = 'amber_projects directory. below this directory,\
                    need a \'data\' folder containing ffncaa frcmod files and ffncaa.in file, \
                    as well as amber_python_utils and amber_python_scripts folders containing appropriate python modules')
parser.add_argument('-sol','--solvate', required = True, help = 'solvate pdb? Do we use an explicit solvent?')
parser.add_argument('-len', '--solvate_box_length', required = False, help = 'solvate_box_length')
parser.add_argument('-io', '--addions', required = False, help = 'do we add ions?')
parser.add_argument('-ff', '--forcefield', required = False, help = 'forcefield used')
#parser.add_argument('-ovw' '--overwrite', required = False, help = 'overwrite, default = False')

args = parser.parse_args()

#assign all inputs to variables
pdbfolder = args.pdb_folder
output_folder = args.output_folder
ambdir = args.amber_projects_dir
solvate = args.solvate
box_length = args.solvate_box_length
add_ions = args.addions
print(add_ions)
forcefield = args.forcefield
print(forcefield)
#overwrite = args.overwrite

import sys
amber_py_utils = os.path.join(ambdir,'amber_python_utils')
sys.path.append(amber_py_utils)

import prep_from_rosetta as prep



if forcefield == None: #set default forcefield
    forcefield = 'leaprc.ff14SB'
    
if solvate == 'Y':
    solvate = True
elif solvate == 'N':
    solvate = False
else:
    raise ValueError('--solvate must be specified as Y for yes and N for no')
    
#if overwrite != None:
#    
#    if overwrite == 'Y':
#        
#        overwrite = True
#        
#    elif overwrite == 'N':
#        
#        overwrite = False
#        
#    else:
#        
#        raise ValueError('overwrite must be specified as Y for yes or N no, or left unspecified')
    
data_dir = os.path.join(ambdir,'data')
aa_map_filepath = os.path.join(data_dir,'rosetta_amb_code_map_short.csv') #setup path to aa_map
#the name of the aa_map is fixed, can edit if you want a different name
#Suggested that one leaves the file and name as it is because the mapping
#is critical to renaming noncanonical amino acids from rosetta nomenclature to amber nomenclature

pdb_names = os.listdir(pdbfolder)
default_dir = os.getcwd()
#print(pdb_names)

for i in range(0,len(pdb_names),1):
    pdb_filename = pdb_names[i]
    subfolder_name = pdb_filename[0:-4] + '_prep4amber'
    subfolder = os.path.join(output_folder,subfolder_name)
    
    if os.path.exists(subfolder):
        
#        if overwrite == True:
        
        raise ValueError(subfolder + 'exists.')
        
    os.mkdir(subfolder)
    os.chdir(subfolder)
    pdb_filepath = os.path.join(pdbfolder,pdb_filename)
    renam_output = prep.aa_rename_rosetta_to_amber(pdb_filepath = pdb_filepath,
                                                     aa_mapping = aa_map_filepath,
                                                     output_folder = subfolder,
                                                     overwrite = True)
    renam_pdb_name = renam_output[0]
    ncaa_names = renam_output[1] #list of ncaas used to find frcmod files
    
    #since we've changed directory to the subfolder, no need to specify full filepaths at this point
    strip_out_name = renam_pdb_name[0:-4] + '_stripH.pdb'
    prep.strip_h2(renam_pdb_name, strip_out_name, overwrite = True)
    
    amber_out_name = strip_out_name[0:-4] + '_ambered.pdb'
    prep.call_pdb4amber(input_pdb = strip_out_name,
                       output_pdb = amber_out_name,
                       overwrite = True)
    
    reduce_name = amber_out_name[0:-4] + '_reduced.pdb'
    prep.call_reduce(input_pdb = amber_out_name,
                     output_pdb = reduce_name,
                     overwrite = True)
    
    amber_again_name = reduce_name[0:-4] + '_ambered.pdb'
    prep.call_pdb4amber(input_pdb = reduce_name,
                       output_pdb = amber_again_name,
                       overwrite = True)
    
    leap_pdb_filepath = os.path.join(subfolder,amber_again_name)
    ffncaa_folder = os.path.join(data_dir,'ffncaa')
    ffncaa_amberprep = os.path.join(ffncaa_folder,'ffncaa.in')
    
    frcmod_list = []
    
    #construct a list of frcmod files
    for n in range(0,len(ncaa_names),1):
        frcmod_file = ncaa_names[n] + '.frcmod'
        frcmod_list.append(os.path.join(ffncaa_folder,frcmod_file))
    
    tleap_name = prep.auto_tleap(input_pdb = leap_pdb_filepath,
                                ffncaa_dir = ffncaa_folder,
                                frcmod_list = frcmod_list, #filepath for frcmod file
                                amber_prep = ffncaa_amberprep, #filepath for amber prep file 
                                expt_name = subfolder_name, #just a name for the leap file, mdcrd, prmtop files
                                output_dir = subfolder, #Below this point, input variables/options not requiredto be specified
                                overwrite = True,
                                solvate = solvate,
                                solvatebox_length = box_length,
                                addions = add_ions, #generally, we will be adding ions to neutralize the system.
                                               # specify 'Na+' for sodium and 'CL-' for chloride
                               forcefield = forcefield
                   )
    tleap_comm = 'tleap -s -f ' + tleap_name
    subprocess.run(tleap_comm.split(),) #run tleap and make mdcrd and prmtop files
    
    os.chdir(default_dir) #change back to default directory
   